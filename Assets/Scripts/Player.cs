﻿using UnityEngine;

namespace Game
{
[RequireComponent(typeof(Movement))]
public class Player : MonoBehaviour
{
    public bool IsAlive { get; private set; } = true;
    public int KeyCounter { get; private set; } = 0;
    
    private Movement _movement;

    private void Awake()
    {
        _movement = GetComponent<Movement>();
    }

    private void Start()
    {
        Enable();
    }

    public void Enable()
    {
        _movement.enabled = true;
    }

    public void Disable()
    {
        _movement.enabled = false;
    }

    public void Kill()
    {
        IsAlive = false;
    }

    public void PickUpKey()
    {
        KeyCounter++;
    }
}
}