using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedDoorWithKey : ObjectActivator
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            ChangeState();
        }
    }
}
