using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
    [SerializeField] int _targetLevelSceneID;

    public void LoadTargetLevel()
    {
        SceneManager.LoadScene(_targetLevelSceneID);
    }
}
