using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectActivator : MonoBehaviour
{
    public GameObject _object;
    public bool _setActive;

    public void ChangeState()
    {
        _object.SetActive(_setActive);
    }
}
