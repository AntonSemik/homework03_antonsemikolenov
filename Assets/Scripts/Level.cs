﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
public class Level : MonoBehaviour
{
    [Header("Timer")]
    [SerializeField] private bool _timerIsOn;
    [SerializeField] private float _timerValue;
    [SerializeField] private Text _timerView;

    [Header("Objects")]
    [SerializeField] private Player _player;
    [SerializeField] private Exit _exitFromLevel;

    [Header("Keys")]
    [SerializeField] private int _keysRequired; //Changed bool to int counting collected keys
    
    private float _timer = 0;
    private bool _gameIsEnded = false;

    public delegate void GameAction();
    public static event GameAction OnVictory;
    public static event GameAction OnLose;


    private void Awake()
    {
        _timer = _timerValue;
    }

    private void Start()
    {
        _exitFromLevel.Close();
    }

    private void Update()
    {
        if(_gameIsEnded)
            return;
        
        TimerTick();
        LookAtPlayerHealth();
        LookAtPlayerInventory();
        TryCompleteLevel();
    }

    private void TimerTick()
    {
        if(_timerIsOn == false)
            return;
        
        _timer -= Time.deltaTime;
        _timerView.text = $"{_timer:F1}";
        
        if(_timer <= 0)
            Lose();
    }

    private void TryCompleteLevel()
    {
        if(_exitFromLevel.IsOpen == false)
            return;

        var flatExitPosition = new Vector2(_exitFromLevel.transform.position.x, _exitFromLevel.transform.position.z);
        var flatPlayerPosition = new Vector2(_player.transform.position.x, _player.transform.position.z);
        
        if(flatExitPosition == flatPlayerPosition)
            Victory();
    }

    private void LookAtPlayerHealth()
    {
        if(_player.IsAlive)
            return;

        Lose();
        Destroy(_player.gameObject);
    }

    private void LookAtPlayerInventory()
    {
        if(_player.KeyCounter >= _keysRequired) //Checking if player collected enough keys to open the exit
            _exitFromLevel.Open();
    }

    //Both victory and lose shoot corresponding events. This is done to make Level.cs less dependant on other scripts
    public void Victory()
    {
        _gameIsEnded = true;
        _player.Disable();
        
        if(OnVictory != null)
        {
                OnVictory();
        }
    }

    public void Lose()
    {
        _gameIsEnded = true;
        _player.Disable();
        if (OnLose != null)
        {
             OnLose();
        }
    }
}
}