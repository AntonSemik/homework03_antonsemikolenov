using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panels : MonoBehaviour
{
    [SerializeField] private GameObject _victoryPanel;
    [SerializeField] private GameObject _losePanel;

    private void Start()
    {
        Game.Level.OnVictory += OnVictory; //Sub
        Game.Level.OnLose += OnLose;
    }

    void OnVictory()
    {
        _victoryPanel.SetActive(true);
    }

    void OnLose()
    {
        _losePanel.SetActive(true);
    }

    private void OnDestroy()
    {
        Game.Level.OnVictory -= OnVictory; //Unsub
        Game.Level.OnLose -= OnLose;
    }
}
