using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathBlock : ObjectActivator
{

    private void OnTriggerExit(Collider other) //Player left the tile - path blocked
    {
        if (other.CompareTag("Player"))
        {
            ChangeState();
        }
    }

}
